import sqlite3

from cfg import DB_PATH
from flask_restful import Resource, reqparse
from models.user import UserModel
from resources.messages import (
    USER_CREATED,
    USER_PASSWORD_HELP,
    USER_USERNAME_EXISTS,
    USER_USERNAME_HELP,
)


class UserRegisterResource(Resource):
    parser = reqparse.RequestParser()
    parser.add_argument(
        "username",
        type=str,
        required=True,
        help=USER_USERNAME_HELP,
    )
    parser.add_argument(
        "password",
        type=str,
        required=True,
        help=USER_PASSWORD_HELP,
    )

    def post(self):
        args = UserRegisterResource.parser.parse_args(strict=True)
        username = args["username"]

        user = UserModel.find_by_username(username)
        if user:
            return {"message": USER_USERNAME_EXISTS.format(username=username)}, 400

        conn = sqlite3.connect(DB_PATH)
        cur = conn.cursor()

        sql_insert = "INSERT INTO users (username, password) VALUES (?, ?)"
        cur.execute(sql_insert, (username, args["password"]))

        conn.commit()
        conn.close()

        return {"message": USER_CREATED}, 201
