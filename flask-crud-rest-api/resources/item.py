from flask_jwt_extended import jwt_required
from flask_restful import Resource, reqparse
from models.item import ItemModel
from resources.messages import (
    ITEM_CREATED,
    ITEM_DELETED,
    ITEM_NAME_EXISTS,
    ITEM_NOT_FOUND,
    ITEM_PRICE_HELP,
    ITEM_UPDATED,
)


class ItemResource(Resource):
    parser = reqparse.RequestParser()
    parser.add_argument(
        "price",
        type=float,
        required=True,
        help=ITEM_PRICE_HELP,
    )

    def get(self, name):
        try:
            item = ItemModel.find_by_name(name)
        except Exception:
            return {"message": "Unable to get item"}, 500

        if item:
            return {"item": item.to_dict()}
        return {"message": ITEM_NOT_FOUND.format(name=name)}, 404

    @jwt_required()
    def post(self, name):
        try:
            item = ItemModel.find_by_name(name)
        except Exception:
            return {"message": "Unable to check if item already exists"}, 500

        if item:
            return {"message": ITEM_NAME_EXISTS.format(name=name)}, 400

        args = ItemResource.parser.parse_args(strict=True)
        item = ItemModel(name, args["price"])

        try:
            item.insert()
            return {"message": ITEM_CREATED}, 201
        except Exception:
            return {"message": "Unable to insert item into database"}, 500

    @jwt_required()
    def delete(self, name):
        try:
            deleted = ItemModel.delete(name)
        except Exception:
            return {"message": "Unable to delete item"}, 500

        if deleted:
            return {"message": ITEM_DELETED}
        return {"message": ITEM_NOT_FOUND.format(name=name)}, 404

    @jwt_required()
    def put(self, name):
        args = ItemResource.parser.parse_args(strict=True)

        try:
            item = ItemModel.find_by_name(name)
        except Exception:
            return {"message": "Unable to check if item already exists"}, 500

        # Create item
        if item is None:
            item = ItemModel(name, args["price"])
            try:
                item.insert()
                return {"message": ITEM_CREATED}, 201
            except Exception:
                return {"message": "Unable to insert item into database"}, 500

        # Update item
        item.price = args["price"]
        try:
            item.update()
            return {"message": ITEM_UPDATED}
        except Exception:
            return {"message": "Unable to update item"}, 500
