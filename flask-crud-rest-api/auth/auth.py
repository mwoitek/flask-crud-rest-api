from auth.messages import USER_LOGIN
from flask import jsonify, request
from flask_jwt_extended import create_access_token, jwt_required
from models.user import UserModel


def login():
    request_data = request.get_json()
    username = request_data.get("username", None)
    password = request_data.get("password", None)

    user = UserModel.authenticate(username, password)
    if user is None:
        return jsonify({"message": USER_LOGIN}), 401

    access_token = create_access_token(identity=user.id)
    return jsonify({"access_token": access_token})


@jwt_required()
def protected():
    user = UserModel.get_current_user()
    return jsonify(
        {
            "id": user.id,
            "username": user.username,
        }
    )
