from auth.messages import JWT_EXPIRED, JWT_NOT_FOUND
from flask import jsonify
from flask_jwt_extended import JWTManager

jwt = JWTManager()


@jwt.expired_token_loader
# pylint: disable=unused-argument
def handle_expired_token(jwt_header, jwt_payload):
    return jsonify({"message": JWT_EXPIRED}), 401


@jwt.unauthorized_loader
# pylint: disable=unused-argument
def handle_no_jwt(reason_jwt_not_found):
    return jsonify({"message": JWT_NOT_FOUND}), 401
