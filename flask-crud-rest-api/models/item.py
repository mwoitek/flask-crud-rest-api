import sqlite3

from cfg import DB_PATH


class ItemModel:
    def __init__(self, name, price):
        self.name = name
        self.price = price

    def to_dict(self):
        return {
            "name": self.name,
            "price": self.price,
        }

    @classmethod
    def find_by_name(cls, name):
        try:
            conn = sqlite3.connect(DB_PATH)
            cur = conn.cursor()

            query = "SELECT * FROM items WHERE name = ?"
            result = cur.execute(query, (name,)).fetchone()

            conn.close()

            item = cls(*result) if result else None
            return item
        except Exception as e:
            raise e

    def insert(self):
        try:
            conn = sqlite3.connect(DB_PATH)
            cur = conn.cursor()

            sql_insert = "INSERT INTO items (name, price) VALUES (?, ?)"
            cur.execute(sql_insert, (self.name, self.price))

            conn.commit()
            conn.close()
        except Exception as e:
            raise e

    # TODO Transform into instance method
    @staticmethod
    def delete(name):
        try:
            conn = sqlite3.connect(DB_PATH)
            cur = conn.cursor()

            sql_delete = "DELETE FROM items WHERE name = ?"
            cur.execute(sql_delete, (name,))
            deleted = cur.rowcount == 1

            conn.commit()
            conn.close()

            return deleted
        except Exception as e:
            raise e

    def update(self):
        try:
            conn = sqlite3.connect(DB_PATH)
            cur = conn.cursor()

            sql_update = "UPDATE items SET price = ? WHERE name = ?"
            cur.execute(sql_update, (self.price, self.name))

            conn.commit()
            conn.close()
        except Exception as e:
            raise e
