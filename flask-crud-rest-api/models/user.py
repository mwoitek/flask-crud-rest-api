import sqlite3

from cfg import DB_PATH
from flask_jwt_extended import get_jwt_identity


class UserModel:
    def __init__(self, _id, username, password):
        self.id = _id
        self.username = username
        self.password = password

    @classmethod
    def find_by_username(cls, username):
        conn = sqlite3.connect(DB_PATH)
        cur = conn.cursor()

        query = "SELECT * FROM users WHERE username = ?"
        query_results = cur.execute(query, (username,))

        result = query_results.fetchone()
        user = cls(*result) if result else None

        conn.close()
        return user

    @classmethod
    def find_by_id(cls, _id):
        conn = sqlite3.connect(DB_PATH)
        cur = conn.cursor()

        query = "SELECT * FROM users WHERE id = ?"
        query_results = cur.execute(query, (_id,))

        result = query_results.fetchone()
        user = cls(*result) if result else None

        conn.close()
        return user

    @classmethod
    def authenticate(cls, username, password):
        user = cls.find_by_username(username)
        if user and user.password == password:
            return user
        return None

    @classmethod
    def get_current_user(cls):
        _id = get_jwt_identity()
        return cls.find_by_id(_id)
