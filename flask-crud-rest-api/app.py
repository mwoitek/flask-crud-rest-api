import os

from auth import auth
from auth.jwt import jwt
from cfg import PORT
from flask import Flask
from flask_restful import Api
from resources.item import ItemResource
from resources.item_list import ItemListResource
from resources.user_register import UserRegisterResource

app = Flask(__name__)

# Setup Flask-JWT-Extended
app.config["JWT_SECRET_KEY"] = os.environ["JWT_SECRET_KEY"]
app.config["PROPAGATE_EXCEPTIONS"] = True
jwt.init_app(app)

app.add_url_rule("/login", view_func=auth.login, methods=["POST"])
app.add_url_rule("/protected", view_func=auth.protected)

api = Api(app)

api.add_resource(ItemListResource, "/items")
api.add_resource(ItemResource, "/item/<string:name>")
api.add_resource(UserRegisterResource, "/register")

if __name__ == "__main__":
    app.run(port=PORT)
