from test.cfg import ROOT_URL, TEST_ITEMS

import requests
from models.item import ItemModel

url = f"{ROOT_URL}/items"


def test_get():
    # Create test items so we always have something to retrieve
    for test_item in TEST_ITEMS:
        item = ItemModel(*test_item.values())
        item.insert()

    # Retrieve items successfully
    response = requests.get(url)
    assert response.status_code == 200

    items = response.json().get("items", None)
    assert items is not None
    assert items[-len(TEST_ITEMS) :] == TEST_ITEMS

    # Delete test items
    for test_item in TEST_ITEMS:
        ItemModel.delete(test_item["name"])
