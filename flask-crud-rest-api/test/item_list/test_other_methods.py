from test.cfg import ROOT_URL

import requests

url = f"{ROOT_URL}/items"


def check_invalid_method(method):
    response = requests.request(method, url)
    assert response.status_code == 405


def test_other_methods():
    # Test methods that are not allowed
    check_invalid_method("DELETE")
    check_invalid_method("POST")
    check_invalid_method("PUT")
