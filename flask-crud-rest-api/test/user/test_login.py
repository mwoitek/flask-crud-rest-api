from test.cfg import ROOT_URL, TEST_USER
from test.user.test_signup import delete_user, signup_user

import requests
from auth.messages import JWT_NOT_FOUND, USER_LOGIN

urls = {
    "login": f"{ROOT_URL}/login",
    "protected": f"{ROOT_URL}/protected",
    "register": f"{ROOT_URL}/register",
}
headers = {"Content-Type": "application/json"}


def test_login():
    # Create test user
    signup_user(urls["register"], TEST_USER)

    # Successful login
    response = requests.post(
        urls["login"],
        headers=headers,
        json=TEST_USER,
    )
    assert response.status_code == 200

    access_token = response.json().get("access_token", None)
    assert access_token is not None

    # Access protected route
    headers["Authorization"] = f"Bearer {access_token}"
    response = requests.get(urls["protected"], headers=headers)
    assert response.status_code == 200

    username = response.json().get("username", None)
    assert username is not None
    assert username == TEST_USER["username"]

    # Delete test user
    delete_user(username)

    # Unsuccessful login
    del headers["Authorization"]
    response = requests.post(
        urls["login"],
        headers=headers,
        json=TEST_USER,
    )
    assert response.status_code == 401

    message = response.json().get("message", None)
    assert message is not None
    assert message == USER_LOGIN

    # Cannot access protected route
    response = requests.get(urls["protected"], headers=headers)
    assert response.status_code == 401

    message = response.json().get("message", None)
    assert message is not None
    assert message == JWT_NOT_FOUND


def get_authorization_header():
    signup_user(urls["register"], TEST_USER)
    response = requests.post(
        urls["login"],
        headers=headers,
        json=TEST_USER,
    )
    access_token = response.json().get("access_token")
    return f"Bearer {access_token}"
