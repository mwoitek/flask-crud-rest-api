import sqlite3
from test.cfg import ROOT_URL, TEST_USER

import requests
from cfg import DB_PATH
from resources.messages import (
    USER_CREATED,
    USER_PASSWORD_HELP,
    USER_USERNAME_EXISTS,
    USER_USERNAME_HELP,
)

url = f"{ROOT_URL}/register"
headers = {"Content-Type": "application/json"}


def signup_user(url, user_data):
    return requests.post(
        url,
        headers=headers,
        json=user_data,
    )


def delete_user(username):
    conn = sqlite3.connect(DB_PATH)
    cur = conn.cursor()

    sql_delete = "DELETE FROM users WHERE username = ?"
    cur.execute(sql_delete, (username,))

    conn.commit()
    conn.close()


def check_missing_data(column):
    wrong_user_data = TEST_USER.copy()
    del wrong_user_data[column]

    response = signup_user(url, wrong_user_data)
    assert response.status_code == 400

    message = response.json().get("message", None)
    assert message is not None

    message_content = message.get(column, None)
    assert message_content is not None
    if column == "username":
        assert message_content == USER_USERNAME_HELP
    elif column == "password":
        assert message_content == USER_PASSWORD_HELP


def test_signup():
    # Successful signup
    response = signup_user(url, TEST_USER)
    assert response.status_code == 201

    message = response.json().get("message", None)
    assert message is not None
    assert message == USER_CREATED

    # Bad request: username already exists
    response = signup_user(url, TEST_USER)
    assert response.status_code == 400

    message = response.json().get("message", None)
    assert message is not None
    username = TEST_USER["username"]
    assert message == USER_USERNAME_EXISTS.format(username=username)

    delete_user(username)

    # Bad request: username is missing
    check_missing_data("username")

    # Bad request: password is missing
    check_missing_data("password")
