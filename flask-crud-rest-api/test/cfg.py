from cfg import PORT

ROOT_URL = f"http://127.0.0.1:{PORT}"

TEST_USER = {
    "username": "test-username",
    "password": "test-password",
}

TEST_ITEM = {
    "name": "Test Item",
    "price": 100.0,
}

TEST_ITEMS = [
    {
        "name": "Test Item 1",
        "price": 100.0,
    },
    {
        "name": "Test Item 2",
        "price": 200.0,
    },
]
