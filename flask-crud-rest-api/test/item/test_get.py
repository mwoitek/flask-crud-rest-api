from test.cfg import ROOT_URL, TEST_ITEM
from urllib.parse import urlparse

import requests
from models.item import ItemModel
from resources.messages import ITEM_NOT_FOUND

item_data = TEST_ITEM.copy()
item_name = item_data.pop("name")

url = urlparse(f"{ROOT_URL}/item/{item_name}").geturl()


def test_get():
    # Create test item so we can retrieve it
    item = ItemModel(*TEST_ITEM.values())
    item.insert()

    # Retrieve item successfully
    response = requests.get(url)
    assert response.status_code == 200

    item_data_response = response.json().get("item", None)
    assert item_data_response is not None
    assert item_data_response == TEST_ITEM

    # Delete test item
    ItemModel.delete(item_name)

    # Try to retrieve an item that does not exist
    response = requests.get(url)
    assert response.status_code == 404

    message = response.json().get("message", None)
    assert message is not None
    assert message == ITEM_NOT_FOUND.format(name=item_name)
