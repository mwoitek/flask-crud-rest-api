from test.cfg import ROOT_URL, TEST_ITEM, TEST_USER
from test.user.test_login import get_authorization_header
from test.user.test_signup import delete_user
from urllib.parse import urlparse

import requests
from auth.messages import JWT_NOT_FOUND
from models.item import ItemModel
from resources.messages import ITEM_CREATED, ITEM_NAME_EXISTS, ITEM_PRICE_HELP

item_data = TEST_ITEM.copy()
item_name = item_data.pop("name")

url = urlparse(f"{ROOT_URL}/item/{item_name}").geturl()
headers = {"Content-Type": "application/json"}


def test_post():
    # Try to create item without authorization
    response = requests.post(
        url,
        headers=headers,
        json=item_data,
    )
    assert response.status_code == 401

    message = response.json().get("message", None)
    assert message is not None
    assert message == JWT_NOT_FOUND

    # Create test user and set authorization header
    headers["Authorization"] = get_authorization_header()

    # Create test item
    response = requests.post(
        url,
        headers=headers,
        json=item_data,
    )
    assert response.status_code == 201

    message = response.json().get("message", None)
    assert message is not None
    assert message == ITEM_CREATED

    # Try to create item with same name
    response = requests.post(
        url,
        headers=headers,
        json=item_data,
    )
    assert response.status_code == 400

    message = response.json().get("message", None)
    assert message is not None
    assert message == ITEM_NAME_EXISTS.format(name=item_name)

    # Delete test item
    ItemModel.delete(item_name)

    # Try to create item with no price
    response = requests.post(
        url,
        headers=headers,
        json={},
    )
    assert response.status_code == 400

    message = response.json().get("message", None)
    assert message is not None

    message_content = message.get("price", None)
    assert message_content is not None
    assert message_content == ITEM_PRICE_HELP

    # Delete test user
    delete_user(TEST_USER["username"])
