from test.cfg import ROOT_URL, TEST_ITEM, TEST_USER
from test.user.test_login import get_authorization_header
from test.user.test_signup import delete_user
from urllib.parse import urlparse

import requests
from auth.messages import JWT_NOT_FOUND
from models.item import ItemModel
from resources.messages import ITEM_DELETED, ITEM_NOT_FOUND

item_name = TEST_ITEM["name"]

url = urlparse(f"{ROOT_URL}/item/{item_name}").geturl()
headers = {}


def test_delete():
    # Create test item so we can delete it
    item = ItemModel(*TEST_ITEM.values())
    item.insert()

    # Try to delete item without authorization
    response = requests.delete(url, headers=headers)
    assert response.status_code == 401

    message = response.json().get("message", None)
    assert message is not None
    assert message == JWT_NOT_FOUND

    # Create test user and set authorization header
    headers["Authorization"] = get_authorization_header()

    # Delete item successfully
    response = requests.delete(url, headers=headers)
    assert response.status_code == 200

    message = response.json().get("message", None)
    assert message is not None
    assert message == ITEM_DELETED

    # Try to delete an item that does not exist
    response = requests.delete(url, headers=headers)
    assert response.status_code == 404

    message = response.json().get("message", None)
    assert message is not None
    assert message == ITEM_NOT_FOUND.format(name=item_name)

    # Delete test user
    delete_user(TEST_USER["username"])
