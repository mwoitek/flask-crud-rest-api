#!/bin/bash

script_path=$(realpath "$0")
script_dir=$(dirname "$script_path")
. "${script_dir}/set_pythonpath.sh"

cd "$root_dir" || return

python_script_path="${PYTHONPATH}/app.py"
pipenv run python "$python_script_path" >/dev/null 2>&1 &
pid=$!

tests_dir="${PYTHONPATH}/test"
pipenv --bare run pytest "$tests_dir"

kill -KILL "$pid"
