#!/bin/bash

script_path=$(realpath "${BASH_SOURCE[0]}")
script_dir=$(dirname "$script_path")
root_dir=$(dirname "$script_dir")

PYTHONPATH="${root_dir}/$(basename "$root_dir")"
export PYTHONPATH
