import json
import subprocess
from pathlib import Path

jls_path_key = "jedi.executable.command"
jls_path = subprocess.run(
    "which jedi-language-server",
    capture_output=True,
    shell=True,
    check=True,
    text=True,
).stdout.strip()

scripts_dir = Path(__file__).resolve().parent
vim_dir = scripts_dir.parent / ".vim"

if not vim_dir.exists():
    subprocess.run(f"mkdir -p {vim_dir}", shell=True, check=True)

config_path = vim_dir / "coc-settings.json"

if not config_path.exists():
    with open(config_path, "w") as config_file:
        json.dump({jls_path_key: jls_path}, config_file)
else:
    with open(config_path, "r") as config_file:
        config_dict = json.load(config_file)
    config_dict[jls_path_key] = jls_path
    with open(config_path, "w") as config_file:
        json.dump(config_dict, config_file)
subprocess.run(
    f"prettier --write --loglevel silent {config_path}",
    shell=True,
    check=True,
)
