#!/bin/bash

script_path=$(realpath "$0")
script_dir=$(dirname "$script_path")
. "${script_dir}/set_pythonpath.sh"

db_path="${root_dir}/data.sqlite"
[[ -e $db_path ]] && rm -f "$db_path"

python_script_path="${PYTHONPATH}/db/create_tables.py"
cd "$root_dir" || return
pipenv --bare run python "$python_script_path"
