#!/bin/bash

script_path=$(realpath "$0")
script_dir=$(dirname "$script_path")
. "${script_dir}/set_pythonpath.sh"

python_script_path="${PYTHONPATH}/app.py"
cd "$root_dir" || return
pipenv --bare run python "$python_script_path"
